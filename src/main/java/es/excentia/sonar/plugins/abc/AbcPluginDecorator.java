/*
 * SonarQube ABC Metric Plugin
 *
 * Copyright (C) 2013 eXcentia Consultoria S.L.
 * All rights reserved.
 *
 * contact@excentia.es
 */
package es.excentia.sonar.plugins.abc;

import japa.parser.TokenMgrError;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sonar.api.batch.Decorator;
import org.sonar.api.batch.DecoratorContext;
import org.sonar.api.batch.DependedUpon;
import org.sonar.api.batch.DependsUpon;
import org.sonar.api.batch.fs.FileSystem;
import org.sonar.api.config.Settings;
import org.sonar.api.measures.CoreMetrics;
import org.sonar.api.measures.Measure;
import org.sonar.api.measures.Metric;
import org.sonar.api.measures.RangeDistributionBuilder;
import org.sonar.api.resources.Project;
import org.sonar.api.resources.Qualifiers;
import org.sonar.api.resources.Resource;

import es.excentia.sonar.plugins.abc.util.AbcUtils;

/**
 * Abc Decorator
 */
public class AbcPluginDecorator implements Decorator {

  private static final String JAVA_VERSION = "java.version";
  private static final String SONAR_JAVA_SOURCE = "sonar.java.source";
  private static final String[] JAVA_SUPPORTED_VERSION = new String[] { "1.5", "1.6", "1.7" };
  private static final String NOT_SUPPORTED_WARNING = " not supported by ABC parser. It is possible that some results are not correct.";

  // Log to print messages
  private static final Logger LOG = LoggerFactory.getLogger(AbcPluginDecorator.class);

  // Range of graphic to print abc by class
  private static final Integer[] BOTTOM_CLASSES = { 0, 30, 60, 90 };

  // Range of graphic to print abc by method
  private static final Integer[] BOTTOM_METHODS = { 0, 15, 30, 45 };

  // Class distribution
  private final RangeDistributionBuilder builderClass = new RangeDistributionBuilder(AbcMetrics.CLASS_ABC_DISTRIBUTION, BOTTOM_CLASSES);
  // Method distribution
  private final RangeDistributionBuilder builderMethod = new RangeDistributionBuilder(AbcMetrics.METHOD_ABC_DISTRIBUTION, BOTTOM_METHODS);

  private final Settings settings;
  private final FileSystem fileSystem;

  /**
   * Constructor
   * 
   * @param settings
   * @param fileSystem
   */
  public AbcPluginDecorator(Settings settings, FileSystem fileSystem) {
    this.settings = settings;
    this.fileSystem = fileSystem;
  }

  /**
   * @return the builderClass
   */
  public final RangeDistributionBuilder getBuilderClass() {
    return builderClass;
  }

  /**
   * @return the builderMethod
   */
  public final RangeDistributionBuilder getBuilderMethod() {
    return builderMethod;
  }

  /**
   * @param value
   * @param chances
   * @return true if the value contains any of the chances
   */
  public static final boolean containsAny(String value, String[] chances) {
    boolean contains = false;

    for (String chance : chances) {
      if (value.contains(chance)) {
        contains = true;
        break;
      }
    }

    return contains;
  }

  /**
   * Log information about Java version
   */
  private void logJavaVersion() {
    // Java version can be found in two different properties
    String sonarJavaSourceVersion = settings.getString(SONAR_JAVA_SOURCE);
    String javaVersion = System.getProperty(JAVA_VERSION);

    LOG.debug(String.format("Abc metric: %s = %s", SONAR_JAVA_SOURCE, sonarJavaSourceVersion));
    LOG.debug(String.format("Abc metric: %s = %s", JAVA_VERSION, javaVersion));

    if (StringUtils.isNotBlank(sonarJavaSourceVersion) && !containsAny(sonarJavaSourceVersion, JAVA_SUPPORTED_VERSION)) {
      LOG.warn("sonar.java.source: " + sonarJavaSourceVersion + NOT_SUPPORTED_WARNING);
    } else if (StringUtils.isNotBlank(javaVersion) && !containsAny(javaVersion, JAVA_SUPPORTED_VERSION)) {
      LOG.warn("java.version: " + javaVersion + NOT_SUPPORTED_WARNING);
    }
  }

  /**
   * Execute decorator only when we have a Java project or the plugin is enabled
   */
  public final boolean shouldExecuteOnProject(Project project) {
    boolean shouldExecuteAbc = false;

    // ENABLED property
    boolean isAbcEnabled = settings.getBoolean(AbcPlugin.PROPKEY_ENABLED);

    if (isAbcEnabled) {
      // Project modules
      List<Project> modules = project.getModules();

      if (modules.isEmpty()) {
        // Java project
        if (fileSystem.languages().contains(AbcUtils.JAVA_LANGUAGE_KEY)) {
          shouldExecuteAbc = true;
          logJavaVersion();
        }
      }
      // Project with modules must be executed always
      else {
        shouldExecuteAbc = true;
      }
    }

    return shouldExecuteAbc;
  }

  /**
   * Necessary metrics
   * 
   * @return metrics to print
   */
  @DependsUpon
  public static List<Metric> getAbsoluteMetrics() {
    // Create metric list with my metrics
    List<Metric> metrics = new ArrayList<Metric>();

    metrics.add(CoreMetrics.LINES);
    metrics.add(CoreMetrics.NCLOC);
    metrics.add(CoreMetrics.FUNCTIONS);
    metrics.add(CoreMetrics.CLASSES);
    metrics.add(CoreMetrics.ACCESSORS);
    metrics.add(CoreMetrics.FUNCTION_COMPLEXITY);
    metrics.add(CoreMetrics.CLASS_COMPLEXITY);

    return metrics;
  }

  /**
   * Result metrics
   * 
   * @return CCI metric
   */
  @DependedUpon
  public final List<Metric> getResultMetrics() {
    return AbcMetrics.getMetricsStatic();
  }

  /**
   * 
   * Calculate and save metrics: abc, abc average class and abc average method
   * 
   * @param resource
   * @param context
   */
  public final void saveMetrics(Resource resource, DecoratorContext context) {
    // Abc Metric of a class
    Measure measure = new Measure(AbcMetrics.ABC, (double) calculateAbcMetric(resource, context));
    context.saveMeasure(measure);

    if (context.getMeasure(CoreMetrics.CLASSES) == null) {
      // Abc Average Metric of a class is 0.0
      Measure measureMeanClass = new Measure(AbcMetrics.ABC_AVERAGE_CLASS, 0.0);
      context.saveMeasure(measureMeanClass);
    } else if (context.getMeasure(CoreMetrics.CLASSES).getValue() == 0.0 || context.getMeasure(AbcMetrics.ABC).getValue() == 0.0) {
      // Abc Average Metric of a class is 0.0
      Measure measureMeanClass = new Measure(AbcMetrics.ABC_AVERAGE_CLASS, 0.0);
      context.saveMeasure(measureMeanClass);
    } else {
      // Abc Average Metric of a class
      Measure measureMeanClass = new Measure(AbcMetrics.ABC_AVERAGE_CLASS, context.getMeasure(AbcMetrics.ABC).getValue()
        / context.getMeasure(CoreMetrics.CLASSES).getValue());
      context.saveMeasure(measureMeanClass);
    }

    if (context.getMeasure(CoreMetrics.FUNCTIONS) == null || context.getMeasure(CoreMetrics.ACCESSORS).getValue() == null) {
      // Abc Average Metric of a method is 0.0
      Measure measureMeanMethod = new Measure(AbcMetrics.ABC_AVERAGE_METHOD, 0.0);
      context.saveMeasure(measureMeanMethod);
    } else if ((context.getMeasure(CoreMetrics.FUNCTIONS).getValue() == 0.0 && context.getMeasure(CoreMetrics.ACCESSORS).getValue() == 0.0)
      || context.getMeasure(AbcMetrics.ABC).getValue() == 0.0) {
      // Abc Average Metric of a method is 0.0
      Measure measureMeanMethod = new Measure(AbcMetrics.ABC_AVERAGE_METHOD, 0.0);
      context.saveMeasure(measureMeanMethod);
    } else {
      // Abc Average Metric of a method
      Measure measureMeanMethod = new Measure(AbcMetrics.ABC_AVERAGE_METHOD, context.getMeasure(AbcMetrics.ABC).getValue()
        / (context.getMeasure(CoreMetrics.FUNCTIONS).getValue() + context.getMeasure(CoreMetrics.ACCESSORS).getValue()));
      context.saveMeasure(measureMeanMethod);
    }
  }

  /**
   * 
   * Save abc value in every distribution: by class and by method distributions to print bar charts
   * 
   * @param context
   */
  public final void saveDistributionClass(DecoratorContext context) {
    // Obtain abc metric
    Measure measure = context.getMeasure(AbcMetrics.ABC);

    // Abc by class
    if (context.getMeasure(CoreMetrics.CLASSES) == null) {
      getBuilderClass().add(0.0);
    } else if (measure.getValue() == 0.0 || context.getMeasure(CoreMetrics.CLASSES).getValue() == 0.0) {
      getBuilderClass().add(0.0);
    } else {
      for (int i = 0; i < context.getMeasure(CoreMetrics.CLASSES).getValue(); i++) {
        getBuilderClass().add(measure.getValue() / context.getMeasure(CoreMetrics.CLASSES).getValue());
      }
    }

    // Abc by method
    if (context.getMeasure(CoreMetrics.FUNCTIONS) == null || context.getMeasure(CoreMetrics.ACCESSORS).getValue() == null) {
      getBuilderMethod().add(0.0);
    } else if (measure.getValue() == 0.0
      || (context.getMeasure(CoreMetrics.FUNCTIONS).getValue() == 0.0 && context.getMeasure(CoreMetrics.ACCESSORS).getValue() == 0.0)) {
      getBuilderMethod().add(0.0);
    } else {
      getBuilderMethod().add(
          measure.getValue()
            / (context.getMeasure(CoreMetrics.FUNCTIONS).getValue() + context.getMeasure(CoreMetrics.ACCESSORS).getValue()));
    }
  }

  /**
   * 
   * Calculate distribution of metric
   * 
   * @param metric
   * @param context
   */
  public final void calculateDistribution(Metric metric, DecoratorContext context) {
    // Metric in subprojects
    Collection<Measure> measuresClass = context.getChildrenMeasures(metric);

    // If there are measures
    if (measuresClass != null) {
      // Every measure
      for (Measure measure : measuresClass) {
        // Split string by every value with number of counts
        String[] values = measure.getData().split(";");
        // Every number of distribution
        for (int j = 0; j < values.length; j++) {
          // Add a value for every number
          for (int i = 0; i < Integer.parseInt(values[j].split("=")[1]); i++) {
            if (metric.equals(AbcMetrics.CLASS_ABC_DISTRIBUTION)) {
              getBuilderClass().add(Integer.parseInt(values[j].split("=")[0]));
            } else {
              getBuilderMethod().add(Integer.parseInt(values[j].split("=")[0]));
            }
          }
        }
      }
    }
  }

  /**
   * 
   * Save distribution with measure of children
   * 
   * @param context
   */
  public final void saveDistribution(DecoratorContext context) {
    // Calculate distribution of classes
    calculateDistribution(AbcMetrics.CLASS_ABC_DISTRIBUTION, context);

    // Calaculta distribution of methods
    calculateDistribution(AbcMetrics.METHOD_ABC_DISTRIBUTION, context);

    // Save measures to print a bar chart
    Measure distributionClass = getBuilderClass().build(false);
    Measure distributionMethod = getBuilderMethod().build(false);

    if (distributionClass != null) {
      context.saveMeasure(distributionClass);
      LOG.debug("ABC BuilderClass :" + context.getMeasure(AbcMetrics.CLASS_ABC_DISTRIBUTION).getData());
    }

    if (distributionMethod != null) {
      context.saveMeasure(distributionMethod);
      LOG.debug("ABC BuilderMethod :" + context.getMeasure(AbcMetrics.METHOD_ABC_DISTRIBUTION).getData());
    }

    // Clear variables
    getBuilderClass().clear();
    getBuilderMethod().clear();
  }

  /**
   * 
   * Calculate abc for every class. Calculate abc for every package or project add every component for every class.
   * 
   * @param resource
   * @param context
   * @return Norm of abc metric
   */
  public final double calculateAbcMetric(Resource resource, DecoratorContext context) {
    // Metric
    AbcMetric abc = null;
    double norm = 0.0;

    // File
    if (Qualifiers.isFile(resource)) {
      // Obtain Abc Metric with values to assigments, branches and conditions
      abc = AbcMetric.obtainAbcMetric(resource, fileSystem);
    }
    // Package or project
    else {
      // Metric
      abc = new AbcMetric();

      // A in subprojects
      Collection<Measure> measuresA = context.getChildrenMeasures(AbcMetrics.ASSIGMENTS);
      // B in subprojects
      Collection<Measure> measuresB = context.getChildrenMeasures(AbcMetrics.BRANCHS);
      // C in subprojects
      Collection<Measure> measuresC = context.getChildrenMeasures(AbcMetrics.CONDITIONS);

      // Add every assigment in every class
      for (Measure measure : measuresA) {
        abc.setAssigmentCount((int) (measure.getValue() + abc.getAssigmentCount()));
      }
      // Add every branch in every class
      for (Measure measure : measuresB) {
        abc.setBranchCount((int) (measure.getValue() + abc.getBranchCount()));
      }
      // Add every condition in every class
      for (Measure measure : measuresC) {
        abc.setConditionCount((int) (measure.getValue() + abc.getConditionCount()));
      }
    }

    if (abc != null) {
      // Components of abc metric
      // A Metric of a class
      Measure measureA = new Measure(AbcMetrics.ASSIGMENTS, (double) abc.getAssigmentCount());
      context.saveMeasure(measureA);
      // B Metric of a class
      Measure measureB = new Measure(AbcMetrics.BRANCHS, (double) abc.getBranchCount());
      context.saveMeasure(measureB);
      // C Metric of a class
      Measure measureC = new Measure(AbcMetrics.CONDITIONS, (double) abc.getConditionCount());
      context.saveMeasure(measureC);

      // Norm of the abc vector
      norm = AbcMetric.calculateAbcNorm(abc);

      LOG.debug("ABC Norma: " + resource.getKey() + " valor: " + norm + " < " + abc.getAssigmentCount() + " ," + abc.getBranchCount()
        + " ," + abc.getConditionCount() + " >");
    }

    // Return norm of vector
    return norm;
  }

  /**
   * Decorate
   */
  @Override
  public final void decorate(Resource resource, DecoratorContext context) {
    try {
      // If resource is a file
      if (Qualifiers.isFile(resource) && !Qualifiers.UNIT_TEST_FILE.equals(resource.getQualifier())) {
        // Save metrics
        saveMetrics(resource, context);

        // Save value in the distribution metric to print a bar chart
        saveDistributionClass(context);
      }
      // If resource is a directory, project or module
      else if (Qualifiers.isDirectory(resource) || Qualifiers.isProject(resource, true)) {

        // Save metrics
        saveMetrics(resource, context);

        // Save value in the distribution metric to print a bar chart
        saveDistribution(context);

        // If LOG is debug then print information and it is a project
        if (LOG.isDebugEnabled() && Qualifiers.isProject(resource, false)) {
          // Print information as abc metric
          AbcUtilPrint.printInformation(resource, context);
        }
      }
    } catch (TokenMgrError exception) {
      LOG.error(String.format("Error %s. Skipping decoration of resource %s...", exception.getMessage(), resource.getLongName()), exception);
    } catch (Exception exception) {
      LOG.error(String.format("Error %s. Skipping decoration of resource %s...", exception.getMessage(), resource.getLongName()), exception);
    }
  }
}