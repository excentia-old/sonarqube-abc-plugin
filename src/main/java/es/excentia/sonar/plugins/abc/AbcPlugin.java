/*
 * SonarQube ABC Metric Plugin
 *
 * Copyright (C) 2013 eXcentia Consultoria S.L.
 * All rights reserved.
 *
 * contact@excentia.es
 */
package es.excentia.sonar.plugins.abc;

import java.util.ArrayList;
import java.util.List;

import org.sonar.api.Extension;
import org.sonar.api.Properties;
import org.sonar.api.Property;
import org.sonar.api.PropertyType;
import org.sonar.api.SonarPlugin;

/**
 * Class to implement the gate of entry to sonar
 */
@Properties({
  @Property(key = AbcPlugin.PROPKEY_ENABLED, name = "Enabled", defaultValue = "true", type = PropertyType.BOOLEAN,
      description = "If set to 'false', the plugin will be disabled. By default the plugin will be enabled.", project = true, global = true),
  @Property(key = AbcPlugin.PROPKEY_CLASS_THRESHOLD, name = "Threshold function level", description = "Threshold function level",
      defaultValue = "LOW=15.9; MEDIUM=30.9; HIGH=45.9", project = true, global = true),
  @Property(key = AbcPlugin.PROPKEY_FUNCTION_THRESHOLD, name = "Threshold class level", description = "Threshold class level",
      defaultValue = "LOW=30.9; MEDIUM=60.9; HIGH=90.9", project = true, global = true) })
public class AbcPlugin extends SonarPlugin {

  public static final String PLUGIN_KEY = "abc";
  public static final String PROPKEY_ENABLED = PLUGIN_KEY + ".enabled";
  public static final String PROPKEY_CLASS_THRESHOLD = PLUGIN_KEY + ".threshold_function";
  public static final String PROPKEY_FUNCTION_THRESHOLD = PLUGIN_KEY + ".threshold_class";

  /**
   * Classes of the plugin
   */
  @Override
  public final List<Class<? extends Extension>> getExtensions() {
    List<Class<? extends Extension>> list = new ArrayList<Class<? extends Extension>>();

    list.add(AbcWidgetInformation.class);
    list.add(AbcPluginDecorator.class);
    list.add(AbcMetrics.class);
    list.add(AbcPluginTab.class);

    return list;
  }
}