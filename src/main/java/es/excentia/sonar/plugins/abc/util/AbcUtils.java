/*
 * SonarQube ABC Metric Plugin
 *
 * Copyright (C) 2013 eXcentia Consultoria S.L.
 * All rights reserved.
 *
 * contact@excentia.es
 */
package es.excentia.sonar.plugins.abc.util;

import java.util.List;

import org.sonar.api.measures.Metric;

/**
 * Abc Utility Class
 */
public final class AbcUtils {

  public static final String JAVA_LANGUAGE_KEY = "java";

  private static final AbcUtils INSTANCE = new AbcUtils();

  /**
   * Private constructor
   */
  private AbcUtils() {
  }

  /**
   * @return the instance
   */
  public static AbcUtils getInstance() {
    return INSTANCE;
  }

  /**
   * Return the metric associated to the key
   * 
   * @param key
   * @param metrics
   * @return the metric associated to the key value or null if this key doesn't exist in the list of metrics
   */
  public static Metric obtainMetric(String key, List<Metric> metrics) {
    // Solution
    Metric metricSolution = null;

    // All metrics of the list
    for (Metric metric : metrics) {
      // If there is a metric with the same key
      if (metric.getKey().equals(key)) {
        metricSolution = metric;
        break;
      }
    }

    return metricSolution;
  }
}