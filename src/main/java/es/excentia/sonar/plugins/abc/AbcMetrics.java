/*
 * SonarQube ABC Metric Plugin
 *
 * Copyright (C) 2013 eXcentia Consultoria S.L.
 * All rights reserved.
 *
 * contact@excentia.es
 */
package es.excentia.sonar.plugins.abc;

import java.util.Arrays;
import java.util.List;

import org.sonar.api.measures.Metric;
import org.sonar.api.measures.Metrics;

/**
 * Metrics of the plugin
 */
public class AbcMetrics implements Metrics {

  private static final String ABC_DOMAIN = "ABC";
  public static final String ABC_KEY = "abc";

  /**
   * A Metric
   */
  public static final Metric ASSIGMENTS = new Metric.Builder("assigments", "A Metric", Metric.ValueType.INT)
      .setDescription("A of Abc Metric").setDirection(Metric.DIRECTION_WORST).setQualitative(false).setDomain(ABC_DOMAIN).create();

  /**
   * B Metric
   */
  public static final Metric BRANCHS = new Metric.Builder("branchs", "B Metric", Metric.ValueType.INT).setDescription("B of Abc Metric")
      .setDirection(Metric.DIRECTION_WORST).setQualitative(false).setDomain(ABC_DOMAIN).create();

  /**
   * C Metric
   */
  public static final Metric CONDITIONS = new Metric.Builder("conditions", "C Metric", Metric.ValueType.INT)
      .setDescription("C of Abc Metric").setDirection(Metric.DIRECTION_WORST).setQualitative(false).setDomain(ABC_DOMAIN).create();

  /**
   * Abc Metric
   */
  public static final Metric ABC = new Metric.Builder(ABC_KEY, "ABC Metric", Metric.ValueType.FLOAT).setDescription("ABC Metric")
      .setDirection(Metric.DIRECTION_WORST).setQualitative(false).setDomain(ABC_DOMAIN).create();

  /**
   * Abc Average Metric for class
   */
  public static final Metric ABC_AVERAGE_CLASS = new Metric.Builder("abcAverageClass", "ABC Average Metric for class",
      Metric.ValueType.FLOAT).setDescription("ABC Average Metric for class").setDirection(Metric.DIRECTION_WORST).setQualitative(false)
      .setDomain(ABC_DOMAIN).create();

  /**
   * Abc Average Metric for method
   */
  public static final Metric ABC_AVERAGE_METHOD = new Metric.Builder("abcAverageMethod", "ABC Average Metric for method",
      Metric.ValueType.FLOAT).setDescription("ABC Average Metric for method").setDirection(Metric.DIRECTION_WORST).setQualitative(false)
      .setDomain(ABC_DOMAIN).create();

  /**
   * Class distribution
   */
  public static final Metric CLASS_ABC_DISTRIBUTION = new Metric.Builder("classAbcDistribution", "Class ABC Distribution",
      Metric.ValueType.DISTRIB).setDescription("Class ABC Distribution").setDirection(Metric.DIRECTION_NONE).setQualitative(false)
      .setDomain(ABC_DOMAIN).create();

  /**
   * Methos distribution
   */
  public static final Metric METHOD_ABC_DISTRIBUTION = new Metric.Builder("methodAbcDistribution", "Method ABC Distribution",
      Metric.ValueType.DISTRIB).setDescription("Method ABC Distribution").setDirection(Metric.DIRECTION_NONE).setQualitative(false)
      .setDomain(ABC_DOMAIN).create();

  /**
   * 
   * @return List of metrics
   */
  public static List<Metric> getMetricsStatic() {
    return Arrays.asList(ABC, ASSIGMENTS, BRANCHS, CONDITIONS, ABC_AVERAGE_CLASS, ABC_AVERAGE_METHOD, CLASS_ABC_DISTRIBUTION,
        METHOD_ABC_DISTRIBUTION);
  }

  /**
   * getMetrics() method is defined in the Metrics interface and is used by Sonar to retrieve the list of new Metric
   */
  public final List<Metric> getMetrics() {
    return getMetricsStatic();
  }
}