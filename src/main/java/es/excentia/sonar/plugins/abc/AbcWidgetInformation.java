/*
 * SonarQube ABC Metric Plugin
 *
 * Copyright (C) 2013 eXcentia Consultoria S.L.
 * All rights reserved.
 *
 * contact@excentia.es
 */
package es.excentia.sonar.plugins.abc;

import org.sonar.api.web.AbstractRubyTemplate;
import org.sonar.api.web.Description;
import org.sonar.api.web.RubyRailsWidget;
import org.sonar.api.web.UserRole;
import org.sonar.api.web.WidgetCategory;

/**
 * Widget of the plugin with information
 */
@UserRole(UserRole.USER)
@Description("Abc Plugin Information")
@WidgetCategory("ABC")
public class AbcWidgetInformation extends AbstractRubyTemplate implements RubyRailsWidget {

  /**
   * Get identificator of the widget
   */
  public final String getId() {
    return "AbcPluginInformation";
  }

  /**
   * Get title of the widget
   */
  public final String getTitle() {
    return "Abc Metric Information";
  }

  /**
   * Relative path of file of the widget
   */
  protected final String getTemplatePath() {
    return "/AbcWidgetInformation.html.erb";
  }
}