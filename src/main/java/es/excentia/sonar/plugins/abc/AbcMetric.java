/*
 * SonarQube ABC Metric Plugin
 *
 * Copyright (C) 2013 eXcentia Consultoria S.L.
 * All rights reserved.
 *
 * contact@excentia.es
 */
package es.excentia.sonar.plugins.abc;

import japa.parser.JavaParser;
import japa.parser.ParseException;
import japa.parser.ast.CompilationUnit;
import japa.parser.ast.body.BodyDeclaration;
import japa.parser.ast.body.ClassOrInterfaceDeclaration;
import japa.parser.ast.body.ConstructorDeclaration;
import japa.parser.ast.body.FieldDeclaration;
import japa.parser.ast.body.MethodDeclaration;
import japa.parser.ast.body.TypeDeclaration;
import japa.parser.ast.body.VariableDeclarator;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sonar.api.batch.fs.FilePredicate;
import org.sonar.api.batch.fs.FileSystem;
import org.sonar.api.resources.Resource;

import es.excentia.sonar.plugins.abc.util.AbcUtils;

/**
 * Metric ABC
 * 
 * ABC = <A, B, C>
 * 
 * A = Assigment counts
 * 
 * B = Branch counts
 * 
 * C = Condition counts
 * 
 * 1. Add one to the assignment count for each occurrence of an assignment operator, excluding constant declarations: = *= /= %= += <<= >>=
 * &= |= ^= >>>=
 * 
 * 2. Add one to the assignment count for each occurrence of an increment or decrement operator (prefix or postfix): ++ --
 * 
 * 3. Add one to the branch count for each function call or class method call.
 * 
 * 4. Add one to the branch count for each occurrence of the new operator.
 * 
 * 5. Add one to the condition count for each use of a conditional operator: == != <= >= < >
 * 
 * 6. Add one to the condition count for each use of the following keywords: else case default try catch ?
 * 
 * 7. Add one to the condition count for each unary conditional expression.
 */
public class AbcMetric {

  // LOG
  private static final Logger LOG = LoggerFactory.getLogger(AbcMetric.class);

  // Number of assigment counts
  private double assigmentCount;

  // Number of branch counts
  private double branchCount;

  // Number of condition counts
  private double conditionCount;

  /**
   * @return the assigmentCount
   */
  public final double getAssigmentCount() {
    return assigmentCount;
  }

  /**
   * @param assigmentCount
   *          the assigmentCount to set
   */
  public final void setAssigmentCount(double assigmentCount) {
    this.assigmentCount = assigmentCount;
  }

  /**
   * @return the branchCount
   */
  public final double getBranchCount() {
    return branchCount;
  }

  /**
   * @param branchCount
   *          the branchCount to set
   */
  public final void setBranchCount(double branchCount) {
    this.branchCount = branchCount;
  }

  /**
   * @return the conditionCount
   */
  public final double getConditionCount() {
    return conditionCount;
  }

  /**
   * @param conditionCount
   *          the conditionCount to set
   */
  public final void setConditionCount(double conditionCount) {
    this.conditionCount = conditionCount;
  }

  /**
   * Builder to initialize counts
   */
  public AbcMetric() {
    this.assigmentCount = 0;
    this.branchCount = 0;
    this.conditionCount = 0;
  }

  /**
   * Obtains the ABC metric of a file
   * 
   * @param resource
   * @param fileSystem
   * @return ABC metric or null if something was wrong
   */
  public static AbcMetric obtainAbcMetric(Resource resource, FileSystem fileSystem) {
    // Encoding of the project
    Charset charset = fileSystem.encoding();

    // Path of class
    String fileName = null;
    String resourcePath = resource.getPath();
    LOG.debug("Resource path: " + resourcePath);

    // Looking for the resource...
    FilePredicate query = fileSystem.predicates().hasLanguage(AbcUtils.JAVA_LANGUAGE_KEY);
    Iterable<File> files = fileSystem.files(query);

    for (File formattedFile : files) {
      // Obtains absolute path
      String filePath = formattedFile.getAbsolutePath().replaceAll("\\\\", "/");
      LOG.debug("File path: " + filePath);

      if (filePath.endsWith(resourcePath)) {
        fileName = formattedFile.getAbsolutePath();
        break;
      }
    }

    return calculateAbc(charset, fileName);
  }

  /**
   * Calculate ABC metric for a given file with specified encoding
   * 
   * @param charset
   * @param fileName
   * @return the abc metric
   */
  private static AbcMetric calculateAbc(Charset charset, String fileName) {
    LOG.debug("ABC Clase: " + fileName);
    FileInputStream file = null;

    // ABC metric
    AbcMetric abc = new AbcMetric();

    try {
      if (StringUtils.isNotBlank(fileName)) {
        // creates an input stream for the file to be parsed
        file = new FileInputStream(fileName);

        // read file
        CompilationUnit cuData = JavaParser.parse(file, charset.name());

        // Calculate abc of file
        abc = calculateAbcMetric(cuData);
      }
    } catch (FileNotFoundException exception) {
      LOG.error(exception.getMessage(), exception);
    } catch (ParseException exception) {
      LOG.error(exception.getMessage(), exception);
    } finally {
      try {
        if (file != null) {
          file.close();
        }
      } catch (IOException exception) {
        LOG.error("Error2:" + exception.getMessage(), exception);
      }
    }

    return abc;
  }

  /**
   * 
   * Calculate ABC metric of data(cuData) of a file
   * 
   * @param cuData
   * @return ABC metric
   */
  public static AbcMetric calculateAbcMetric(CompilationUnit cuData) {
    // Abc metric
    AbcMetric abc = new AbcMetric();

    // Class with every component
    List<TypeDeclaration> types = cuData.getTypes();
    if (cuData.getTypes() != null) {
      for (TypeDeclaration type : types) {
        // Members is every member of a class
        List<BodyDeclaration> members = type.getMembers();

        if (type.getMembers() != null) {
          // Every member
          for (BodyDeclaration member : members) {

            // Calculate abc of every member
            AbcMetric abc2 = calculateAbcMetricMember(member);

            // Add every component to abc
            abc.setAssigmentCount(abc2.getAssigmentCount() + abc.getAssigmentCount());
            abc.setBranchCount(abc2.getBranchCount() + abc.getBranchCount());
            abc.setConditionCount(abc2.getConditionCount() + abc.getConditionCount());
          }
        }
      }
    }

    return abc;
  }

  /**
   * 
   * Calculate ABC metric for one member of a class, for example another class
   * 
   * @param member
   * @return ABC metric of one member of a class
   */
  public static AbcMetric calculateAbcMetricMember(BodyDeclaration member) {
    // Abc metric
    AbcMetric abc = new AbcMetric();

    // Methods of the class
    if (member instanceof MethodDeclaration) {
      MethodDeclaration method = (MethodDeclaration) member;

      // Body of every method
      if (method.getBody() != null) {
        // Calculate ABC metric of the body of method
        abc = calculateAbcMetricLine(method.getBody().toString());
      }

      // Atributes of the class with or without comments
    } else if (member instanceof FieldDeclaration) {

      // Exclude constants
      if (member.toString().indexOf("final") == -1 || member.toString().indexOf("static") == -1) {
        // List of attributes
        List<VariableDeclarator> variables = ((FieldDeclaration) member).getVariables();
        // Every attribute
        for (VariableDeclarator variableDeclarator : variables) {
          // Calculate ABC metric of the attribute
          abc = calculateAbcMetricLine(variableDeclarator.toString());
        }
      }

      // Constructor of the class
    } else if (member instanceof ConstructorDeclaration) {
      ConstructorDeclaration constructor = (ConstructorDeclaration) member;

      // Body of constructor
      if (constructor.getBlock() != null) {
        // Calculate ABC metric of the constructor
        abc = calculateAbcMetricLine(constructor.getBlock().toString());
      }

      // Inner class
    } else if (member instanceof ClassOrInterfaceDeclaration) {
      abc = calculateAbcMetricInnerClass((ClassOrInterfaceDeclaration) member);
    }

    return abc;
  }

  /**
   * 
   * Calculate ABC metric of a class
   * 
   * @param member
   * @return ABC metric of a inner class
   */
  public static AbcMetric calculateAbcMetricInnerClass(ClassOrInterfaceDeclaration member) {
    // Abc metric
    AbcMetric abc = new AbcMetric();

    // Members is every member of a class
    List<BodyDeclaration> members2 = member.getMembers();
    for (BodyDeclaration member2 : members2) {
      // Calculate ABC metric of this class
      AbcMetric abc2 = calculateAbcMetricMember(member2);

      // Add every component to total ABC metric
      abc.setAssigmentCount(abc2.getAssigmentCount() + abc.getAssigmentCount());
      abc.setBranchCount(abc2.getBranchCount() + abc.getBranchCount());
      abc.setConditionCount(abc2.getConditionCount() + abc.getConditionCount());

    }

    return abc;
  }

  /**
   * 
   * Calculate ABC metric of a string
   * 
   * @param string
   * @return ABC metric of the string
   */
  public static AbcMetric calculateAbcMetricLine(String string) {
    // Abc metric
    AbcMetric abc = new AbcMetric();

    // Split every line
    StringTokenizer tokens = new StringTokenizer(string, "\n");

    // Read every line
    while (tokens.hasMoreTokens()) {
      String line = tokens.nextToken();

      // Count assigment
      abc.setAssigmentCount(abc.getAssigmentCount() + AbcMetricParser.searchAssigmentPattern(line));
      // Count branch
      abc.setBranchCount(abc.getBranchCount() + AbcMetricParser.searchBranchPattern(line));
      // Count conditions
      abc.setConditionCount(abc.getConditionCount() + AbcMetricParser.searchConditionPattern(line));
    }

    return abc;
  }

  /**
   * Calculate the norm of the vector ABC.
   * 
   * @param abc
   * @return norma
   */
  public static double calculateAbcNorm(AbcMetric abc) {
    // The norm of Abc ||ABC|| is sqrt((A*A)+(B*B)+(C*C))
    return Math.sqrt(((double) abc.getAssigmentCount() * (double) abc.getAssigmentCount())
      + ((double) abc.getBranchCount() * (double) abc.getBranchCount())
      + ((double) abc.getConditionCount() * (double) abc.getConditionCount()));
  }
}