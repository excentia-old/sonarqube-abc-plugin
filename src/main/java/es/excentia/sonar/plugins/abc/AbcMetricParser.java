/*
 * SonarQube ABC Metric Plugin
 *
 * Copyright (C) 2013 eXcentia Consultoria S.L.
 * All rights reserved.
 *
 * contact@excentia.es
 */
package es.excentia.sonar.plugins.abc;

import java.util.StringTokenizer;

import jregex.Matcher;
import jregex.Pattern;

/**
 * Class to parse lines
 */
public final class AbcMetricParser {

  // Attribute to convert class to singleton
  private static final AbcMetricParser INSTANCE = new AbcMetricParser();

  // Constructor
  private AbcMetricParser() {
  }

  /**
   * @return the instance
   */
  public static AbcMetricParser getInstance() {
    return INSTANCE;
  }

  // All
  private static final String[] ALL = { "==|[*/%+&|^]?=|<{2}=|>{2,3}=|[!<>]+=?", "\\+{2}", "\\-{2}", "[.^a-zA-Z0-9]*new[.^a-zA-Z0-9]*",
    "[.^a-zA-Z0-9]*else[.^a-zA-Z0-9]*", "[.^a-zA-Z0-9]*case[.^a-zA-Z0-9]*", "[.^a-zA-Z0-9]*default[.^a-zA-Z0-9]*",
    "[.^a-zA-Z0-9]*try[.^a-zA-Z0-9]*", "[.^a-zA-Z0-9]*catch[.^a-zA-Z0-9]*", "\\?" };

  // Assigment
  private static final String[] ASSIGNMENT_OPERATIONS = { "=", "*=", "/=", "%=", "+=", "<<=", ">>=", "&=", "|=", "^=", ">>>=", "++", "--" };

  // Branch
  private static final String[] BRANCH_OPERATIONS = { "new" };

  // Condition
  private static final String[] CONDITION_OPERATIONS = { "==", "!=", "<=", ">=", "<", ">", "?" };
  private static final String[] CONDITION_OPERATIONS2 = { "else", "case", "default", "try", "catch" };

  /**
   * 
   * Obtain number of assigments:
   * 
   * 1. Add one to the assignment count for each occurrence of an assignment operator, excluding constant declarations: = *= /= %= += <<=
   * >>= &= |= ^= >>>=
   * 
   * 2. Add one to the assignment count for each occurrence of an increment or decrement operator (prefix or postfix): ++ --
   * 
   * @param linea
   * @return number of assigments
   */
  public static double searchAssigmentPattern(String linea) {
    double assigmentCount = 0;
    Pattern patron = null;

    // Exclude constants
    Pattern patron2 = new Pattern("final");
    Pattern patron3 = new Pattern("static");
    Matcher matcher = null;
    Matcher matcher2 = null;
    Matcher matcher3 = null;

    // Every operation
    for (String all : ALL) {
      // All operations
      patron = new Pattern(all);

      matcher = patron.matcher(linea);
      matcher2 = patron2.matcher(linea);
      matcher3 = patron3.matcher(linea);
      // If there is one assigment but it is not a constant
      while (matcher.find() && ( !matcher2.find() || !matcher3.find())) {
        // Every assigment operation
        for (String string : ASSIGNMENT_OPERATIONS) {
          if (string.equals(matcher.group(0))) {
            assigmentCount++;
          }
        }
      }
    }

    return assigmentCount;
  }

  /**
   * 
   * Obtain number of methods of branches:
   * 
   * 3. Add one to the branch count for each function call or class method call.
   * 
   * @param linea
   * @return number of methods of branches
   */
  public static double searchBranchMethodsPattern(String linea) {
    // Call a method v.m(), v.m(a), v.m(v1.m2()), ...
    Pattern patron = new Pattern("\\.[^\\..+]+\\(.*\\)");
    double branchCount = 0;

    Matcher matcher = patron.matcher(linea);

    // If there is a call method add one and look the rest of string
    if (matcher.find()) {
      // Obtain the rest of string
      String aux = linea.substring(matcher.start() + 1, linea.length());
      branchCount = 1 + searchBranchMethodsPattern(aux);
    } else {
      // baseline
      branchCount = 0;
    }

    return branchCount;
  }

  /**
   * Obtain number of branches:
   * 
   * 3. Add one to the branch count for each function call or class method call.
   * 
   * 4. Add one to the branch count for each occurrence of the new operator.
   * 
   * @param linea
   * @return number of branches
   */
  public static double searchBranchPattern(String linea) {
    double branchCount = 0;

    // Methods
    branchCount = branchCount + searchBranchMethodsPattern(linea);

    // All operations
    for (String all : ALL) {

      Pattern patron = new Pattern(all);
      Matcher matcher = patron.matcher(linea);

      // If there is one operation
      while (matcher.find()) {
        // Every branch operation
        for (String string : BRANCH_OPERATIONS) {
          // If there is one branch
          if (string.equals(matcher.group(0))) {
            branchCount++;
          }
        }
      }
    }

    return branchCount;
  }

  /**
   * 
   * Obtain number of unary conditions:
   * 
   * 7. Add one to the condition count for each unary conditional expression.
   * 
   * @param linea
   * @return number of unary conditions
   */
  public static double searchConditionUnaryPattern(String linea) {
    double unaryCount = 0;

    // Conditions to evaluate conditions and negation
    Pattern patron = new Pattern("![.^=]*|&&|\\|\\|");

    // Statements: if, while and switch
    Pattern patron2 = new Pattern("if|while|switch");

    // Conditions to evaluate conditions and and negation
    Matcher matcher = patron.matcher(linea);

    // Statements: if, while and switch
    Matcher matcher2 = patron2.matcher(linea);

    // Split by &&
    StringTokenizer tokensAnd = new StringTokenizer(linea, "&&");
    // Split by ||
    StringTokenizer tokensOr = new StringTokenizer(linea, "||");

    // Statements
    while (matcher2.find()) {
      // Logical conditions
      while (matcher.find()) {
        if ("&&".equals(matcher.group(0))) {
          unaryCount = tokensAnd.countTokens();
        } else if ("||".equals(matcher.group(0))) {
          unaryCount = tokensOr.countTokens();
        } else if ("!".equals(matcher.group(0))) {
          unaryCount++;
        }
      }
    }

    return unaryCount;

  }

  /**
   * Obtain number of conditions:
   * 
   * 5. Add one to the condition count for each use of a conditional operator: == != <= >= < >
   * 
   * 6. Add one to the condition count for each use of the following keywords: else case default try catch ?
   * 
   * @param linea
   * @return number of conditions
   */
  public static double searchConditionPattern(String linea) {
    double conditionCount = 0;
    boolean conditional = false;

    // Statements: if, while and switch
    Pattern patron2 = new Pattern("if|while|switch");

    // Statements: if, while and switch
    Matcher matcher2 = patron2.matcher(linea);

    // If there is a conditional word then it is a conditional operator
    if (matcher2.find()) {
      conditional = true;
    } else {
      conditional = false;
    }

    // All operations
    for (String all : ALL) {

      Pattern patron = new Pattern(all);
      Matcher matcher = patron.matcher(linea);

      // If there is one operation and it is a conditional operation
      while (matcher.find()) {
        // Every condition operation: "==", "!=", "<=", ">=", "<", ">"
        for (String string : CONDITION_OPERATIONS) {
          // If there is one condition in a conditional operation: if,
          // while, switch
          if (conditional && string.equals(matcher.group(0))) {
            conditionCount++;
          }
        }
        // Every condition operation: "else", "case", "default", "try",
        // "catch", "?"
        for (String string : CONDITION_OPERATIONS2) {
          // If there is one condition
          if (string.equals(matcher.group(0))) {
            conditionCount++;
          }
        }
      }
    }

    return conditionCount + searchConditionUnaryPattern(linea);
  }
}