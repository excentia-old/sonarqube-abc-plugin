/*
 * SonarQube ABC Metric Plugin
 *
 * Copyright (C) 2013 eXcentia Consultoria S.L.
 * All rights reserved.
 *
 * contact@excentia.es
 */
package es.excentia.sonar.plugins.abc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sonar.api.batch.DecoratorContext;
import org.sonar.api.measures.CoreMetrics;
import org.sonar.api.resources.Resource;

/**
 * Class to print information
 */
public final class AbcUtilPrint {

  private static final Logger LOG = LoggerFactory.getLogger(AbcUtilPrint.class);

  private static final AbcUtilPrint INSTANCE = new AbcUtilPrint();

  private static final double CUATRO = 4.9;
  private static final double SIETE = 7.9;
  private static final double DIEZ = 10.9;
  private static final double QUINCE = 15.9;
  private static final double VEINTE = 20.9;
  private static final double TREINTA = 30.9;
  private static final double CUARENTA = 45.9;
  private static final double SESENTA = 60.9;
  private static final double CINCUENTA = 50.9;
  private static final double NOVENTA = 90.9;

  // Constructor
  private AbcUtilPrint() {
  }

  /**
   * @return the instance
   */
  public static AbcUtilPrint getInstance() {
    return INSTANCE;
  }

  /**
   * 
   * Print information in debug mode
   * 
   * @param resource
   * @param context
   */
  public static void printInformation(Resource resource, DecoratorContext context) {
    LOG.debug("ABC Not class: " + resource.getKey());

    // If project has lines
    if (context.getMeasure(CoreMetrics.LINES) != null) {
      LOG.debug("ABC Lines: " + context.getMeasure(CoreMetrics.LINES).getValue());
      LOG.debug("ABC Ncloc: " + context.getMeasure(CoreMetrics.NCLOC).getValue());
      LOG.debug("ABC Methods: " + context.getMeasure(CoreMetrics.FUNCTIONS).getValue());
      LOG.debug("ABC Classes: " + context.getMeasure(CoreMetrics.CLASSES).getValue());
      LOG.debug("ABC Function Complexity: " + context.getMeasure(CoreMetrics.FUNCTION_COMPLEXITY).getValue());
      LOG.debug("ABC Class Complexity: " + context.getMeasure(CoreMetrics.CLASS_COMPLEXITY).getValue());
      LOG.debug("ABC Abc Average Class: " + context.getMeasure(AbcMetrics.ABC_AVERAGE_CLASS).getValue());
      LOG.debug("ABC Abc Average Method: " + context.getMeasure(AbcMetrics.ABC_AVERAGE_METHOD).getValue());
      LOG.debug("ABC Abc Total: " + context.getMeasure(AbcMetrics.ABC).getValue());

      obtainRankingClass(context.getMeasure(AbcMetrics.ABC_AVERAGE_CLASS).getValue());
      obtainRankingComplexityClass(context.getMeasure(CoreMetrics.CLASS_COMPLEXITY).getValue());
      obtainRankingMethod(context.getMeasure(AbcMetrics.ABC_AVERAGE_METHOD).getValue());
      obtainRankingComplexityMethod(context.getMeasure(CoreMetrics.FUNCTION_COMPLEXITY).getValue());
    }
  }

  /**
   * Print ranking of complexity class
   * 
   * @param classComplexity
   */
  public static void obtainRankingComplexityClass(Double classComplexity) {
    // Class
    if (classComplexity <= DIEZ) {
      LOG.debug("ABC 22 Ranking Complexity Class: A");
    } else if (DIEZ < classComplexity && classComplexity <= VEINTE) {
      LOG.debug("ABC 22 Ranking Complexity Class: B");
    } else if (VEINTE < classComplexity && classComplexity <= CINCUENTA) {
      LOG.debug("ABC 22 Ranking Complexity Class: C");
    } else if (classComplexity > CINCUENTA) {
      LOG.debug("ABC 22 Ranking Complexity Class: D");
    }
  }

  /**
   * Print ranking of complexity method
   * 
   * @param functionComplexity
   */
  public static void obtainRankingComplexityMethod(Double functionComplexity) {
    // Method
    if (functionComplexity <= CUATRO) {
      LOG.debug("ABC 23 Ranking Complexity Method: A");
    } else if (CUATRO < functionComplexity && functionComplexity <= SIETE) {
      LOG.debug("ABC 23 Ranking Complexity Method: B");
    } else if (SIETE < functionComplexity && functionComplexity <= DIEZ) {
      LOG.debug("ABC 23 Ranking Complexity Method: C");
    } else if (functionComplexity > DIEZ) {
      LOG.debug("ABC 23 Ranking Complexity Method: D");
    }
  }

  /**
   * Print ranking of ABC method
   * 
   * @param abcAverageClass
   */
  public static void obtainRankingClass(Double abcAverageClass) {
    // Class
    if (abcAverageClass <= TREINTA) {
      LOG.debug("ABC 20 Ranking Class: A");
    } else if (TREINTA < abcAverageClass && abcAverageClass <= SESENTA) {
      LOG.debug("ABC 20 Ranking Class: B");
    } else if (SESENTA < abcAverageClass && abcAverageClass <= NOVENTA) {
      LOG.debug("ABC 20 Ranking Class: C");
    } else if (abcAverageClass > NOVENTA) {
      LOG.debug("ABC 20 Ranking Class: D");
    }
  }

  /**
   * Print ranking of ABC class
   * 
   * @param abcAverageMethod
   */
  public static void obtainRankingMethod(Double abcAverageMethod) {
    // Method
    if (abcAverageMethod <= QUINCE) {
      LOG.debug("ABC 21 Ranking Method: A");
    } else if (QUINCE < abcAverageMethod && abcAverageMethod <= TREINTA) {
      LOG.debug("ABC 21 Ranking Method: B");
    } else if (TREINTA < abcAverageMethod && abcAverageMethod <= CUARENTA) {
      LOG.debug("ABC 21 Ranking Method: C");
    } else if (abcAverageMethod > CUARENTA) {
      LOG.debug("ABC 21 Ranking Method: D");
    }
  }
}