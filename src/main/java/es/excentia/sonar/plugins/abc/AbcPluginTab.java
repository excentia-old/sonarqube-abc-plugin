/*
 * SonarQube ABC Metric Plugin
 *
 * Copyright (C) 2013 eXcentia Consultoria S.L.
 * All rights reserved.
 *
 * contact@excentia.es
 */
package es.excentia.sonar.plugins.abc;

import org.sonar.api.resources.Qualifiers;
import org.sonar.api.web.AbstractRubyTemplate;
import org.sonar.api.web.DefaultTab;
import org.sonar.api.web.NavigationSection;
import org.sonar.api.web.ResourceQualifier;
import org.sonar.api.web.RubyRailsPage;
import org.sonar.api.web.UserRole;

/**
 * Abc metric Tab
 */
@NavigationSection(NavigationSection.RESOURCE_TAB)
@ResourceQualifier(Qualifiers.FILE)
@UserRole(UserRole.CODEVIEWER)
@DefaultTab(metrics = { AbcMetrics.ABC_KEY })
public class AbcPluginTab extends AbstractRubyTemplate implements RubyRailsPage {

  /**
   * ABC tab ID
   */
  public final String getId() {
    return "abcTab";
  }

  /**
   * ABC tab Title
   */
  public final String getTitle() {
    return "ABC";
  }

  /**
   * ABC tab Template path
   */
  @Override
  protected final String getTemplatePath() {
    return "/AbcPluginTab.html.erb";
  }
}