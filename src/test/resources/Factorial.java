public class Factorial {
  
  private final Integer value;
  
  public Factorial(Integer value) {
    this.value = value;
  }
  
  public final Integer getValue() {
    return value;
  }
  
  public Integer calculate() {
    return calculate(value);
  }
  
  private Integer calculate(Integer value) {
    if (value == 1) {
      return 1;
    } else {
      return value * new Factorial(value -1).calculate();
    }
  }

}