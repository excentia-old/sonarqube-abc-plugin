/*
 * SonarQube ABC Metric Plugin
 *
 * Copyright (C) 2013 eXcentia Consultoria S.L.
 * All rights reserved.
 *
 * contact@excentia.es
 */
package es.excentia.sonar.plugins.abc;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.PowerMock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.sonar.api.measures.CoreMetrics;
import org.sonar.api.measures.Measure;
import org.sonar.api.resources.Resource;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Resource.class)
public class AbcUtilPrintTest {

  private DecoratorContextSupport context;

  @Before
  public void setUp() {
    context = spy(new DecoratorContextSupport());

    Measure measure = mock(Measure.class);

    when(measure.getValue()).thenReturn(1.0);

    when(context.getMeasure(CoreMetrics.LINES)).thenReturn(measure);
    when(context.getMeasure(CoreMetrics.NCLOC)).thenReturn(measure);
    when(context.getMeasure(CoreMetrics.FUNCTIONS)).thenReturn(measure);
    when(context.getMeasure(CoreMetrics.CLASSES)).thenReturn(measure);
    when(context.getMeasure(CoreMetrics.FUNCTION_COMPLEXITY)).thenReturn(measure);
    when(context.getMeasure(CoreMetrics.CLASS_COMPLEXITY)).thenReturn(measure);
    when(context.getMeasure(AbcMetrics.ABC_AVERAGE_CLASS)).thenReturn(measure);
    when(context.getMeasure(AbcMetrics.ABC_AVERAGE_METHOD)).thenReturn(measure);
    when(context.getMeasure(AbcMetrics.ABC)).thenReturn(measure);
  }

  @Test
  public void testGetInstanceNotNull() {
    assertNotNull("Instance created", AbcUtilPrint.getInstance());
  }

  @Test
  public void testPrintMethods() {
    AbcUtilPrint.obtainRankingComplexityClass(5.0);
    AbcUtilPrint.obtainRankingComplexityClass(15.0);
    AbcUtilPrint.obtainRankingComplexityClass(35.0);
    AbcUtilPrint.obtainRankingComplexityClass(60.0);

    AbcUtilPrint.obtainRankingComplexityMethod(3.0);
    AbcUtilPrint.obtainRankingComplexityMethod(6.0);
    AbcUtilPrint.obtainRankingComplexityMethod(9.0);
    AbcUtilPrint.obtainRankingComplexityMethod(12.0);

    AbcUtilPrint.obtainRankingClass(20.0);
    AbcUtilPrint.obtainRankingClass(40.0);
    AbcUtilPrint.obtainRankingClass(70.0);
    AbcUtilPrint.obtainRankingClass(100.0);

    AbcUtilPrint.obtainRankingMethod(12.0);
    AbcUtilPrint.obtainRankingMethod(22.0);
    AbcUtilPrint.obtainRankingMethod(32.0);
    AbcUtilPrint.obtainRankingMethod(46.0);
  }

  @Test
  public void testPrintInformation() {
    // Resource for a class when you need getKey because is a final method
    Resource resource = PowerMock.createPartialMock(Resource.class, "getKey");
    EasyMock.expect(resource.getKey()).andReturn("key");
    PowerMock.replay(resource);

    AbcUtilPrint.printInformation(resource, context);
  }
}