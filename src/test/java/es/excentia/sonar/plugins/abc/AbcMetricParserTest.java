/*
 * SonarQube ABC Metric Plugin
 *
 * Copyright (C) 2013 eXcentia Consultoria S.L.
 * All rights reserved.
 *
 * contact@excentia.es
 */
package es.excentia.sonar.plugins.abc;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

public class AbcMetricParserTest {

  @Test
  public void testGetInstanceNotNull() {
    assertNotNull("Instance created", AbcMetricParser.getInstance());
  }
}