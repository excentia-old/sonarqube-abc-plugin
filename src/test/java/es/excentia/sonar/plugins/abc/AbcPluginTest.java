/*
 * SonarQube ABC Metric Plugin
 *
 * Copyright (C) 2013 eXcentia Consultoria S.L.
 * All rights reserved.
 *
 * contact@excentia.es
 */
package es.excentia.sonar.plugins.abc;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.sonar.api.Extension;

public class AbcPluginTest {

  private final AbcPlugin plugin = new AbcPlugin();

  @Test
  public void testGetLicensedPluginExtensions() {
    List<Class<? extends Extension>> list = new ArrayList<Class<? extends Extension>>();
    list.add(AbcWidgetInformation.class);
    list.add(AbcPluginDecorator.class);
    list.add(AbcMetrics.class);
    list.add(AbcPluginTab.class);

    assertEquals("Plugin extensions must contain all extensions created", plugin.getExtensions(), list);
  }
}