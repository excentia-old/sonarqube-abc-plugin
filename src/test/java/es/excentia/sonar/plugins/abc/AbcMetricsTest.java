/*
 * SonarQube ABC Metric Plugin
 *
 * Copyright (C) 2013 eXcentia Consultoria S.L.
 * All rights reserved.
 *
 * contact@excentia.es
 */
package es.excentia.sonar.plugins.abc;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.Test;

public class AbcMetricsTest {

  private final AbcMetrics metrics = new AbcMetrics();

  @Test
  public void testGetMetrics() {
    assertEquals("Metrics list must contain all metrics created", metrics.getMetrics(), Arrays.asList(AbcMetrics.ABC,
        AbcMetrics.ASSIGMENTS, AbcMetrics.BRANCHS, AbcMetrics.CONDITIONS, AbcMetrics.ABC_AVERAGE_CLASS, AbcMetrics.ABC_AVERAGE_METHOD,
        AbcMetrics.CLASS_ABC_DISTRIBUTION, AbcMetrics.METHOD_ABC_DISTRIBUTION));
  }
}