/*
 * SonarQube ABC Metric Plugin
 *
 * Copyright (C) 2013 eXcentia Consultoria S.L.
 * All rights reserved.
 *
 * contact@excentia.es
 */
package es.excentia.sonar.plugins.abc.util;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;

import es.excentia.sonar.plugins.abc.AbcMetrics;

public class AbcUtilsTest {

  @Test
  public void testGetInstanceNotNull() {
    assertNotNull("Instance created", AbcUtils.getInstance());
  }

  @Test
  public void testGetMetricFound() {
    assertNotNull("Metric ABC found", AbcUtils.obtainMetric("abc", AbcMetrics.getMetricsStatic()));
  }

  @Test
  public void testGetMetricNotFound() {
    assertNull("Metric not found", AbcUtils.obtainMetric("cba", AbcMetrics.getMetricsStatic()));
  }
}