/*
 * SonarQube ABC Metric Plugin
 *
 * Copyright (C) 2013 eXcentia Consultoria S.L.
 * All rights reserved.
 *
 * contact@excentia.es
 */
package es.excentia.sonar.plugins.abc;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.sonar.api.batch.DecoratorContext;
import org.sonar.api.batch.Event;
import org.sonar.api.design.Dependency;
import org.sonar.api.measures.Measure;
import org.sonar.api.measures.MeasuresFilter;
import org.sonar.api.measures.Metric;
import org.sonar.api.resources.Project;
import org.sonar.api.resources.Resource;
import org.sonar.api.rules.Violation;
import org.sonar.api.violations.ViolationQuery;

/**
 * Based on DecoratorContextSupport
 */
public class DecoratorContextSupport implements DecoratorContext {

  private Metric metric;
  private Double value;
  private String data;

  /**
   * Returns the metric
   * 
   * @return the metric
   */
  public final Metric getMetric() {
    return metric;
  }

  /**
   * @param metric
   *          the metric to set
   */
  public final void setMetric(Metric metric) {
    this.metric = metric;
  }

  /**
   * Returns the metric's value
   * 
   * @return the metric's value
   */
  public final Double getValue() {
    return value;
  }

  /**
   * @param value
   *          the value to set
   */
  public final void setValue(Double value) {
    this.value = value;
  }

  /**
   * Returns the metric's string value
   * 
   * @return the metric's string value
   */
  public final String getData() {
    return data;
  }

  /**
   * @param data
   *          the data to set
   */
  public final void setData(String data) {
    this.data = data;
  }

  /**
   * Stores a measure into the context
   * 
   * @param measure
   *          measure to save
   * @return decorator context
   */
  public DecoratorContext saveMeasure(Measure measure) {
    if (measure.hasData()) {
      saveMeasure(measure.getMetric(), measure.getData());
    } else {
      saveMeasure(measure.getMetric(), measure.getValue());
    }
    return this;
  }

  /**
   * Stores a metric into the context
   * 
   * @param metric
   *          metric to save
   * @param value
   *          metric's value
   * @return decorator context
   */
  public DecoratorContext saveMeasure(Metric metric, Double value) {
    this.metric = metric;
    this.value = value;
    return this;
  }

  /**
   * Stores a metric into the context
   * 
   * @param metric
   *          metric to save
   * @param value
   *          metric's value
   * @return decorator context
   */
  public DecoratorContext saveMeasure(Metric metric, String value) {
    this.metric = metric;
    this.data = value;
    return this;
  }

  /**
   * @param arg0
   * @param arg1
   * @param arg2
   * @param date
   * @return null
   */
  public Event createEvent(String arg0, String arg1, String arg2, Date date) {
    return null;
  }

  /**
   * 
   */
  public void deleteEvent(Event event) {
    // doesn't do anything
  }

  /**
   * @return null
   */
  public List<DecoratorContext> getChildren() {
    return null;
  }

  /**
   * @param measuresFilter
   * @return null
   */
  @SuppressWarnings("rawtypes")
  public Collection<Measure> getChildrenMeasures(MeasuresFilter measuresFilter) {
    return null;
  }

  /**
   * @param metric
   * @return null
   */
  public Collection<Measure> getChildrenMeasures(Metric metric) {
    return null;
  }

  /**
   * @return null
   */
  public Set<Dependency> getDependencies() {
    return null;
  }

  /**
   * @return null
   */
  public List<Event> getEvents() {
    return null;
  }

  /**
   * @return null
   */
  public Collection<Dependency> getIncomingDependencies() {
    return null;
  }

  /**
   * @param metric
   * @return null
   */
  public Measure getMeasure(Metric metric) {
    return null;
  }

  /**
   * @param measuresFilter
   * @return null
   */
  public <M> M getMeasures(MeasuresFilter<M> measuresFilter) {
    return null;
  }

  /**
   * @return null
   */
  public Collection<Dependency> getOutgoingDependencies() {
    return null;
  }

  /**
   * @return null
   */
  public Project getProject() {
    return null;
  }

  /**
   * @return null
   */
  public Resource getResource() {
    return null;
  }

  /**
   * @return null
   */
  public List<Violation> getViolations() {
    return null;
  }

  /**
   * @param violationQuery
   * @return null
   */
  public List<Violation> getViolations(ViolationQuery violationQuery) {
    return null;
  }

  /**
   * @param dependency
   * @return null
   */
  public Dependency saveDependency(Dependency dependency) {
    return null;
  }

  /**
   * @param violation
   * @return null
   */
  public DecoratorContext saveViolation(Violation violation) {
    return null;
  }

  /**
   * @param violation
   * @param bool
   * @return null
   */
  public DecoratorContext saveViolation(Violation violation, boolean bool) {
    return null;
  }
}