/*
 * SonarQube ABC Metric Plugin
 *
 * Copyright (C) 2013 eXcentia Consultoria S.L.
 * All rights reserved.
 *
 * contact@excentia.es
 */
package es.excentia.sonar.plugins.abc;

import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Abc metric Tab Test
 */
public class AbcPluginTabTest {

  private static AbcPluginTab tab;

  @BeforeClass
  public static void setUp() {
    tab = new AbcPluginTab();
  }

  @Test
  public void testWidgetsId() {
    assertEquals("Tab ID must be abcTab", tab.getId(), "abcTab");
  }

  @Test
  public void testWidgetsTitle() {
    assertEquals("Tab title must be ABC", tab.getTitle(), "ABC");
  }

  @Test
  public void testWidgetsPath() {
    assertEquals("Tab template path must be /AbcPluginTab.html.erb", tab.getTemplatePath(), "/AbcPluginTab.html.erb");
  }
}