/*
 * SonarQube ABC Metric Plugin
 *
 * Copyright (C) 2013 eXcentia Consultoria S.L.
 * All rights reserved.
 *
 * contact@excentia.es
 */
package es.excentia.sonar.plugins.abc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.io.File;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import org.easymock.EasyMock;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.PowerMock;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.sonar.api.batch.fs.FilePredicate;
import org.sonar.api.batch.fs.FilePredicates;
import org.sonar.api.batch.fs.FileSystem;
import org.sonar.api.config.Settings;
import org.sonar.api.measures.CoreMetrics;
import org.sonar.api.measures.Measure;
import org.sonar.api.measures.Metric;
import org.sonar.api.resources.Project;
import org.sonar.api.resources.Qualifiers;
import org.sonar.api.resources.Resource;
import org.sonar.api.resources.Scopes;

import es.excentia.sonar.plugins.abc.util.AbcUtils;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Resource.class)
@PowerMockIgnore({ "javax.management.*", "javax.xml.parsers.*", "com.sun.org.apache.xerces.internal.jaxp.*", "ch.qos.logback.*",
  "org.slf4j.*", "javax.crypto.*" })
public class AbcPluginDecoratorTest {

  private static final String FACTORIAL_SOURCE_JAVA = "src/test/resources/Factorial.java";
  private static Resource resourceProject;
  private static Resource resourceModule;
  private static Resource resourcePackage;
  private static DecoratorContextSupport context;
  private static AbcPluginDecorator decorator;
  private static Project project;
  private static FileSystem fileSystem;
  private static FilePredicates filePredicates;
  private static FilePredicate predicate;

  private static Measure abc;
  private static Measure classes;
  private static Measure functions;
  private static Measure accessors;

  private static Measure a;
  private static Measure b;
  private static Measure c;

  private static Measure distribution_classes;
  private static Measure distribution_method;

  private static Collection<Measure> measuresA;
  private static Collection<Measure> measuresB;
  private static Collection<Measure> measuresC;

  private static Collection<Measure> measuresClass;
  private static Collection<Measure> measuresMethod;

  private static Settings settings;

  @Test
  public void testGetTemplateWhenValidLicenseSet() {
    AbcWidgetInformation widget = new AbcWidgetInformation();

    String template = widget.getTemplatePath();
    assertNotNull(template);
    assertFalse(template.contains("nvalid"));
  }

  @BeforeClass
  public static void setUp() {
    // licenser configuration
    project = mock(Project.class);

    resourceProject = mock(Resource.class);
    when(resourceProject.getQualifier()).thenReturn(Qualifiers.PROJECT);

    resourceModule = mock(Resource.class);
    when(resourceModule.getQualifier()).thenReturn(Qualifiers.MODULE);

    resourcePackage = mock(Resource.class);
    when(resourcePackage.getQualifier()).thenReturn(Qualifiers.DIRECTORY);

    SortedSet<String> langs = new TreeSet<String>();
    langs.add(AbcUtils.JAVA_LANGUAGE_KEY);

    fileSystem = mock(FileSystem.class);

    when(fileSystem.encoding()).thenReturn(Charset.defaultCharset());
    when(fileSystem.languages()).thenReturn(langs);

    filePredicates = mock(FilePredicates.class);
    predicate = mock(FilePredicate.class);

    when(filePredicates.hasLanguage(AbcUtils.JAVA_LANGUAGE_KEY)).thenReturn(predicate);
    when(fileSystem.predicates()).thenReturn(filePredicates);
    when(fileSystem.files(predicate)).thenReturn(Arrays.asList(new File(".")));

    context = spy(new DecoratorContextSupport());
    when(context.getProject()).thenReturn(project);

    abc = mock(Measure.class);
    when(abc.getValue()).thenReturn(6.16);
    when(context.getMeasure(AbcMetrics.ABC)).thenReturn(abc);

    classes = mock(Measure.class);
    when(classes.getValue()).thenReturn(8.0);
    when(context.getMeasure(CoreMetrics.CLASSES)).thenReturn(classes);

    functions = mock(Measure.class);
    when(functions.getValue()).thenReturn(20.0);
    when(context.getMeasure(CoreMetrics.FUNCTIONS)).thenReturn(functions);

    accessors = mock(Measure.class);
    when(accessors.getValue()).thenReturn(20.0);
    when(context.getMeasure(CoreMetrics.ACCESSORS)).thenReturn(accessors);

    a = mock(Measure.class);
    when(a.getValue()).thenReturn((double) 2);
    when(context.getMeasure(AbcMetrics.ASSIGMENTS)).thenReturn(a);

    measuresA = new ArrayList<Measure>();
    measuresA.add(a);
    when(context.getChildrenMeasures(AbcMetrics.ASSIGMENTS)).thenReturn(measuresA);

    b = mock(Measure.class);
    when(b.getValue()).thenReturn((double) 3);
    when(context.getMeasure(AbcMetrics.BRANCHS)).thenReturn(a);

    measuresB = new ArrayList<Measure>();
    measuresB.add(b);
    when(context.getChildrenMeasures(AbcMetrics.BRANCHS)).thenReturn(measuresB);

    c = mock(Measure.class);
    when(c.getValue()).thenReturn((double) 5);
    when(context.getMeasure(AbcMetrics.CONDITIONS)).thenReturn(c);

    measuresC = new ArrayList<Measure>();
    measuresC.add(c);
    when(context.getChildrenMeasures(AbcMetrics.CONDITIONS)).thenReturn(measuresC);

    distribution_classes = mock(Measure.class);
    when(context.getMeasure(AbcMetrics.CLASS_ABC_DISTRIBUTION)).thenReturn(distribution_classes);

    distribution_method = mock(Measure.class);
    when(context.getMeasure(AbcMetrics.METHOD_ABC_DISTRIBUTION)).thenReturn(distribution_method);

    settings = new Settings();
    Map<String, String> properties = new HashMap<String, String>();
    properties.put("sonar.java.source", "1.5");
    properties.put(AbcPlugin.PROPKEY_ENABLED, "true");
    System.setProperty("java.version", "1.5");
    settings.addProperties(properties);

    decorator = new AbcPluginDecorator(settings, fileSystem);
  }

  @Test
  public void testIsPluginDisabled() {
    settings.setProperty(AbcPlugin.PROPKEY_ENABLED, "false");
    Project project = new Project("prueba");

    assertFalse("Plugin disabled", decorator.shouldExecuteOnProject(project));
    settings.setProperty(AbcPlugin.PROPKEY_ENABLED, "true");
  }

  @Test
  public void testIfLanguageIsSupported() {
    Project project = new Project("prueba");

    assertTrue("Java source 1.5", decorator.shouldExecuteOnProject(project));
  }

  @Test
  public void testNullSonarJavaSourceAndJavaVersionUnsupported() {
    Project project = new Project("prueba");

    // Test different java versions with null value for sonar.java.version
    System.setProperty("java.version", "1.6");
    String nullString = null;

    settings.setProperty("sonar.java.source", nullString);
    decorator = new AbcPluginDecorator(settings, fileSystem);

    assertTrue("Java source 1.6", decorator.shouldExecuteOnProject(project));
  }

  @Test
  public void testNullSonarJavaSourceAndJavaVersionSupported() {
    Project project = new Project("prueba");

    // Test different java versions with null value for sonar.java.version
    System.setProperty("java.version", "1.5");
    String nullString = null;

    settings.setProperty("sonar.java.source", nullString);
    decorator = new AbcPluginDecorator(settings, fileSystem);

    assertTrue(decorator.shouldExecuteOnProject(project));
  }

  @Test
  public void testSonarJavaSourceUnsupported() {
    Project project = new Project("prueba");

    System.setProperty("java.version", "1.6");

    settings.setProperty("sonar.java.source", "1.6");
    decorator = new AbcPluginDecorator(settings, fileSystem);

    assertTrue(decorator.shouldExecuteOnProject(project));
  }

  @Test
  public void testSonarJavaSourceSupported() {
    Project project = new Project("prueba");

    System.setProperty("java.version", "1.6");

    settings.setProperty("sonar.java.source", "1.5");
    decorator = new AbcPluginDecorator(settings, fileSystem);

    assertTrue(decorator.shouldExecuteOnProject(project));
  }

  /**
   * Round the number value with 1 digit precision
   * 
   * @param number
   *          the value to round
   * @return the number rounded
   */
  private final double round(double number) {
    double rounded;

    rounded = Math.rint(number * 10) / 10;

    return rounded;
  }

  /**
   * Test to method obtainAbcMetric
   */
  @Test
  public void obtainAbcMetricTest() {
    // Resource for a class when you need getKey because is a final method
    Resource resource = PowerMock.createPartialMock(Resource.class, "getPath");
    EasyMock.expect(resource.getPath()).andReturn(FACTORIAL_SOURCE_JAVA).andReturn(FACTORIAL_SOURCE_JAVA).andReturn(FACTORIAL_SOURCE_JAVA);
    PowerMock.replay(resource);

    when(fileSystem.files(predicate)).thenReturn(Arrays.asList(new File(FACTORIAL_SOURCE_JAVA)));

    AbcMetric abc = AbcMetric.obtainAbcMetric(resource, fileSystem);

    assertNotNull("Metric ABC not null", abc);
    assertEquals("Measure A", Double.valueOf(abc.getAssigmentCount()), Double.valueOf(1.0));
    assertEquals("Measure B", Double.valueOf(abc.getBranchCount()), Double.valueOf(2.0));
    assertEquals("Measure C", Double.valueOf(abc.getConditionCount()), Double.valueOf(2.0));
  }

  /**
   * Test to method obtainAbcMetric
   */
  @Test
  public void obtainAbcMetricFailTest() {
    // Resource for a class when you need getKey because is a final method
    Resource resource = PowerMock.createPartialMock(Resource.class, "getPath");
    EasyMock.expect(resource.getPath()).andReturn("kk.txt");
    PowerMock.replay(resource);

    AbcMetric abc = AbcMetric.obtainAbcMetric(resource, fileSystem);
    assertEquals(Double.valueOf(abc.getAssigmentCount()), Double.valueOf(0.0));
  }

  /**
   * Test to method searchAssigmentPattern
   */
  @Test
  public void searchAssigmentPatternTest() {
    String linea = new String("private static final String ABC_DOMAIN = Abc;");
    assertEquals(Double.valueOf(AbcMetricParser.searchAssigmentPattern(linea)), Double.valueOf(0.0));
    linea = new String("private static String ABC_DOMAIN = Abc;");
    assertEquals(Double.valueOf(AbcMetricParser.searchAssigmentPattern(linea)), Double.valueOf(1.0));
    linea = "private static String[] ASSIGNMENT_OPERATIONS={\"=\", \"\\*=\", \"/=\", \"%=\", \"\\+=\", \"<<=\", \">>=\", \"&=\", \"\\|=\", \"\\^=\", \">>>=\", \"++\", \"--\"};";
    assertEquals(Double.valueOf(AbcMetricParser.searchAssigmentPattern(linea)), Double.valueOf(14.0));
  }

  /**
   * Test to method searchBranchMethodsPattern
   */
  @Test
  public void searchBranchMethodsPatternTest() {
    String linea = new String("'m.find()'");
    assertEquals(Double.valueOf(AbcMetricParser.searchBranchMethodsPattern(linea)), Double.valueOf(1.0));
    linea = new String("'System.out.print()'");
    assertEquals(Double.valueOf(AbcMetricParser.searchBranchMethodsPattern(linea)), Double.valueOf(1.0));
    linea = new String("'m.find(s)'");
    assertEquals(Double.valueOf(AbcMetricParser.searchBranchMethodsPattern(linea)), Double.valueOf(1.0));
    linea = new String("m.find(start.m())'");
    assertEquals(Double.valueOf(AbcMetricParser.searchBranchMethodsPattern(linea)), Double.valueOf(2.0));
    linea = new String("m.find(start.m(start))'");
    assertEquals(Double.valueOf(AbcMetricParser.searchBranchMethodsPattern(linea)), Double.valueOf(2.0));
    linea = new String("m.find(start.m3(start.m2()))'");
    assertEquals(Double.valueOf(AbcMetricParser.searchBranchMethodsPattern(linea)), Double.valueOf(3.0));
    linea = new String("import es.excentia.sonar.plugins.utils.AbcUtils;");
    assertEquals(Double.valueOf(AbcMetricParser.searchBranchMethodsPattern(linea)), Double.valueOf(0.0));
  }

  /**
   * Test to method searchBranchPattern
   */
  @Test
  public void searchBranchPatternTest() {
    String linea = new String("'m.find()'");
    assertEquals(Double.valueOf(AbcMetricParser.searchBranchPattern(linea)), Double.valueOf(1.0));
    linea = new String("'System.out.print()'");
    assertEquals(Double.valueOf(AbcMetricParser.searchBranchPattern(linea)), Double.valueOf(1.0));
    linea = new String("'m.find(s)'");
    assertEquals(Double.valueOf(AbcMetricParser.searchBranchPattern(linea)), Double.valueOf(1.0));
    linea = new String("m.find(start)'");
    assertEquals(Double.valueOf(AbcMetricParser.searchBranchPattern(linea)), Double.valueOf(1.0));
    linea = new String("m.find(start.m())'");
    assertEquals(Double.valueOf(AbcMetricParser.searchBranchPattern(linea)), Double.valueOf(2.0));
    linea = new String("m.find(start.m(start))'");
    assertEquals(Double.valueOf(AbcMetricParser.searchBranchPattern(linea)), Double.valueOf(2.0));
    linea = new String("m.find(start.m3(start.m2()))'");
    assertEquals(Double.valueOf(AbcMetricParser.searchBranchPattern(linea)), Double.valueOf(3.0));
    linea = new String("new");
    assertEquals(Double.valueOf(AbcMetricParser.searchBranchPattern(linea)), Double.valueOf(1.0));
    linea = new String("newf");
    assertEquals(Double.valueOf(AbcMetricParser.searchBranchPattern(linea)), Double.valueOf(0.0));
    linea = new String("anew");
    assertEquals(Double.valueOf(AbcMetricParser.searchBranchPattern(linea)), Double.valueOf(0.0));
  }

  /**
   * Test to method searchConditionPattern
   */
  @Test
  public void searchConditionPatternTest() {
    String linea = "aelse";
    assertEquals(Double.valueOf(AbcMetricParser.searchConditionPattern(linea)), Double.valueOf(0.0));
    linea = "else case default try catch = defaultValue aelse";
    assertEquals(Double.valueOf(AbcMetricParser.searchConditionPattern(linea)), Double.valueOf(5.0));
    linea = "defaultValue =";
    assertEquals(Double.valueOf(AbcMetricParser.searchConditionPattern(linea)), Double.valueOf(0.0));
    linea = "if(a>b)";
    assertEquals(Double.valueOf(AbcMetricParser.searchConditionPattern(linea)), Double.valueOf(1.0));
    linea = "<";
    assertEquals(Double.valueOf(AbcMetricParser.searchConditionPattern(linea)), Double.valueOf(0.0));
    linea = "List<Metric>";
    assertEquals(Double.valueOf(AbcMetricParser.searchConditionPattern(linea)), Double.valueOf(0.0));
  }

  /**
   * Test to method searchConditionUnaryPattern
   */
  @Test
  public void searchConditionUnaryPatternTest() {
    String linea = new String("if(a==b)");
    assertEquals(Double.valueOf(AbcMetricParser.searchConditionUnaryPattern(linea)), Double.valueOf(0.0));
    linea = "if(a&&b)";
    assertEquals(Double.valueOf(AbcMetricParser.searchConditionUnaryPattern(linea)), Double.valueOf(2.0));
    linea = "if(a>b)";
    assertEquals(Double.valueOf(AbcMetricParser.searchConditionUnaryPattern(linea)), Double.valueOf(0.0));
    linea = "if(a&&b&&c)";
    assertEquals(Double.valueOf(AbcMetricParser.searchConditionUnaryPattern(linea)), Double.valueOf(3.0));
    linea = "switch(a==b)";
    assertEquals(Double.valueOf(AbcMetricParser.searchConditionUnaryPattern(linea)), Double.valueOf(0.0));
    linea = "switch(!a)";
    assertEquals(Double.valueOf(AbcMetricParser.searchConditionUnaryPattern(linea)), Double.valueOf(1.0));
    linea = "while(!a)";
    assertEquals(Double.valueOf(AbcMetricParser.searchConditionUnaryPattern(linea)), Double.valueOf(1.0));
    linea = "while(a||b)";
    assertEquals(Double.valueOf(AbcMetricParser.searchConditionUnaryPattern(linea)), Double.valueOf(2.0));
    linea = "while(a&&b&&c&&d&&e)";
    assertEquals(Double.valueOf(AbcMetricParser.searchConditionUnaryPattern(linea)), Double.valueOf(5.0));
  }

  /**
   * Test to method calculateAbcNorm
   */
  @Test
  public void calculateAbcNormTest() {
    AbcMetric abc = new AbcMetric();
    abc.setAssigmentCount(2);
    abc.setBranchCount(3);
    abc.setConditionCount(1);
    // sqrt(2*2+3*3+1*1)=3.7416575
    assertTrue(AbcMetric.calculateAbcNorm(abc) >= 3.74);

    abc.setAssigmentCount(26325);
    abc.setBranchCount(74946);
    abc.setConditionCount(20006);
    assertTrue(AbcMetric.calculateAbcNorm(abc) >= 81915.496);

    abc.setAssigmentCount(263250000);
    abc.setBranchCount(749460000);
    abc.setConditionCount(2000600000);
    assertTrue(AbcMetric.calculateAbcNorm(abc) >= 2.15);

  }

  /**
   * Test to method getAbsoluteMetrics
   */
  @Test
  public void getAbsoluteMetricsTest() {
    assertTrue(AbcPluginDecorator.getAbsoluteMetrics().contains(CoreMetrics.NCLOC));
  }

  /**
   * Test to method getResultMetrics
   */
  @Test
  public void getResultMetricsTest() {
    assertTrue(decorator.getResultMetrics().contains(AbcMetrics.ABC));
  }

  /**
   * Test to method obtainMetric
   */
  @SuppressWarnings("static-access")
  @Test
  public void obtainMetricTest() {
    List<Metric> metrics = decorator.getAbsoluteMetrics();
    assertEquals(AbcUtils.obtainMetric("ncloc", metrics).getKey(), "ncloc");
    assertNull(AbcUtils.obtainMetric("abc", metrics));
  }

  /**
   * Test to method saveMetrics
   */
  @Test
  public void saveMetricsTest() {
    decorator.saveMetrics(resourcePackage, context);

    // The last metric is abcAverageMethod
    // 6.16/40=0.154
    assertEquals(context.getValue(), Double.valueOf(round(0.154)));

    when(classes.getValue()).thenReturn(0.0);
    when(functions.getValue()).thenReturn(0.0);
    when(accessors.getValue()).thenReturn(0.0);

    decorator.saveMetrics(resourcePackage, context);
    assertEquals(context.getValue(), Double.valueOf(0.0));

    when(classes.getValue()).thenReturn(8.0);
    when(functions.getValue()).thenReturn(20.0);
    when(accessors.getValue()).thenReturn(20.0);
  }

  /**
   * Test to method saveDistribution
   */
  @Test
  public void saveDistributionTest() {
    // Distribution is empty
    assertTrue(decorator.getBuilderClass().isEmpty());

    decorator.saveDistributionClass(context);

    Measure distributionClass = decorator.getBuilderClass().build(false);
    measuresClass = new ArrayList<Measure>();
    measuresClass.add(distributionClass);
    when(context.getChildrenMeasures(AbcMetrics.CLASS_ABC_DISTRIBUTION)).thenReturn(measuresClass);

    // Distribution is not empty
    assertFalse(decorator.getBuilderClass().isEmpty());

    decorator.saveDistribution(context);

    // Distribution is empty
    assertTrue(decorator.getBuilderClass().isEmpty());
  }

  /**
   * Test to method calculateDistribution
   */
  @Test
  public void calculateDistributionTest() {

    // Distribution is empty
    assertTrue(decorator.getBuilderClass().isEmpty());

    decorator.saveDistributionClass(context);

    Measure distributionClass = decorator.getBuilderClass().build(false);
    measuresClass = new ArrayList<Measure>();
    measuresClass.add(distributionClass);
    when(context.getChildrenMeasures(AbcMetrics.CLASS_ABC_DISTRIBUTION)).thenReturn(measuresClass);

    decorator.calculateDistribution(AbcMetrics.CLASS_ABC_DISTRIBUTION, context);

    // Distribution is not empty
    assertFalse(decorator.getBuilderClass().isEmpty());

    Measure distributionMethod = decorator.getBuilderMethod().build(false);
    measuresMethod = new ArrayList<Measure>();
    measuresMethod.add(distributionMethod);
    when(context.getChildrenMeasures(AbcMetrics.METHOD_ABC_DISTRIBUTION)).thenReturn(measuresMethod);

    decorator.calculateDistribution(AbcMetrics.METHOD_ABC_DISTRIBUTION, context);

    // Distribution is not empty
    assertFalse(decorator.getBuilderMethod().isEmpty());
  }

  /**
   * Test to method saveDistributionClass
   */
  @Test
  public void saveDistributionClassTest() {
    // Save a distribution
    decorator.saveDistributionClass(context);

    // Distribution is not empty
    assertFalse(decorator.getBuilderClass().isEmpty());
    assertFalse(decorator.getBuilderMethod().isEmpty());

    // Clear distribution
    decorator.getBuilderClass().clear();
    decorator.getBuilderMethod().clear();
    // Distributions is empty
    assertTrue(decorator.getBuilderClass().isEmpty());

    // Classes equal 0
    when(classes.getValue()).thenReturn(0.0);

    // Distribution is 0 because classes is 0
    decorator.saveDistributionClass(context);
    assertFalse(decorator.getBuilderClass().isEmpty());

    // Classes is 8
    when(classes.getValue()).thenReturn(8.0);

    // Functions equal 0
    when(functions.getValue()).thenReturn(0.0);

    // Distribution is 0 because funtions is 0
    decorator.saveDistributionClass(context);
    assertFalse(decorator.getBuilderClass().isEmpty());

    // Functions is 20
    when(functions.getValue()).thenReturn(20.0);
  }

  /**
   * Test to method calculateAbcMetric with a class
   */
  @Test
  public void calculateAbcMetricClassTest() {
    Resource resource = PowerMock.createPartialMock(Resource.class, "getPath");
    EasyMock.expect(resource.getQualifier()).andReturn(Qualifiers.FILE);
    EasyMock.expect(resource.getPath()).andReturn(FACTORIAL_SOURCE_JAVA).andReturn(FACTORIAL_SOURCE_JAVA).andReturn(FACTORIAL_SOURCE_JAVA);
    PowerMock.replay(resource);

    when(fileSystem.files(predicate)).thenReturn(Arrays.asList(new File(FACTORIAL_SOURCE_JAVA)));

    assertEquals("ABC Metric", Double.valueOf(decorator.calculateAbcMetric(resource, context)), Double.valueOf(3.0));
  }

  /**
   * Test to method calculateAbcMetric with a project
   */
  @Test
  public void calculateAbcMetricProjectTest() {
    // Sqrt(3*3+2*2+5*5)=6.164414002968976
    assertTrue("ABC Metric", decorator.calculateAbcMetric(resourceProject, context) >= 6.16);
  }

  /**
   * Test to method decorate with resource is a package
   */
  @Test
  public void decoratePackageTest() {

    decorator.decorate(resourcePackage, context);

    // 6.16/40=0.154
    assertEquals(context.getValue(), Double.valueOf(round(0.154)));
  }

  /**
   * Test to method decorate with resource is a project
   */
  @Test
  public void decorateProjectTest() {

    decorator.saveDistributionClass(context);

    Measure distributionClass = decorator.getBuilderClass().build(false);
    measuresClass = new ArrayList<Measure>();
    measuresClass.add(distributionClass);

    when(context.getChildrenMeasures(AbcMetrics.CLASS_ABC_DISTRIBUTION)).thenReturn(measuresClass);

    decorator.decorate(resourceProject, context);

    // 6.16/40=0.154
    assertEquals(context.getValue(), Double.valueOf(round(0.154)));
  }

  /**
   * Test to method decorate with resource is a class
   */
  @Test
  public void decorateClassTest() {
    Resource resource = PowerMock.createPartialMock(Resource.class, "getScope", "getPath", "getQualifier");
    EasyMock.expect(resource.getScope()).andReturn(Scopes.FILE).andReturn(Scopes.FILE).andReturn(Scopes.FILE);
    EasyMock.expect(resource.getQualifier()).andReturn(Qualifiers.FILE).andReturn(Qualifiers.FILE).andReturn(Qualifiers.FILE)
        .andReturn(Qualifiers.FILE).andReturn(Qualifiers.FILE).andReturn(Qualifiers.FILE);
    EasyMock.expect(resource.getPath()).andReturn(FACTORIAL_SOURCE_JAVA).andReturn(FACTORIAL_SOURCE_JAVA).andReturn(FACTORIAL_SOURCE_JAVA);
    PowerMock.replay(resource);

    decorator.decorate(resource, context);

    // 6.16/40=0.154
    assertEquals(context.getValue(), Double.valueOf(round(0.154)));
  }

  @Test
  public void containsAny() {
    assertTrue("Contains", AbcPluginDecorator.containsAny(" asdf", new String[] { " " }));
    assertTrue("Contains", AbcPluginDecorator.containsAny(" asdf", new String[] { "df" }));
    assertTrue("Contains", AbcPluginDecorator.containsAny(" asdf", new String[] { " asdf", "k" }));
    assertTrue("Contains", AbcPluginDecorator.containsAny(" asdf", new String[] { "asdf" }));
    assertTrue("Contains", AbcPluginDecorator.containsAny(" asdf", new String[] { "2", "r", "s" }));
    assertFalse("Contains", AbcPluginDecorator.containsAny(" asdf", new String[] { "2", "r", "t" }));
    assertFalse("Contains", AbcPluginDecorator.containsAny(" asdf", new String[] {}));
  }
}