/*
 * SonarQube ABC Metric Plugin
 *
 * Copyright (C) 2013 eXcentia Consultoria S.L.
 * All rights reserved.
 *
 * contact@excentia.es
 */
package es.excentia.sonar.plugins.abc;

import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;

public class AbcPluginWidgetsTest {

  private static AbcWidgetInformation abcWidget;

  /**
   * Called once before test methods
   */
  @BeforeClass
  public static void setUp() {
    abcWidget = new AbcWidgetInformation();
  }

  @Test
  public void testWidgetsId() {
    assertEquals("Abc widget ID", abcWidget.getId(), "AbcPluginInformation");
  }

  @Test
  public void testWidgetsTitle() {
    assertEquals("Abc widget title", abcWidget.getTitle(), "Abc Metric Information");
  }

  @Test
  public void testWidgetsPath() {
    assertEquals("Abc widget template path", abcWidget.getTemplatePath(), "/AbcWidgetInformation.html.erb");
  }
}