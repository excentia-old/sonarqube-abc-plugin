# Features #

ABC Metric is a plugin for SonarQube that calculates a new size metric based on ABC Concept (Assignment, Branches, Conditions) for java projects.

The plugin calculates ABC metric for each class, package or Java project. Metric is based on the idea that imperative languajes like C or Pascal use variables to conduct its funcionality.

It has three main funcionalities: storage, calls and conditions.

ABC metric calculates an array with three components:

* Assignment: a data transfer to a variable (A)
* Branch: method calls (B)
* Condition: logical or boolean condition (C)

ABC metric value is got calculating ABC array norm.

## Interpretation and classification

After conducting exhaustive experiments with very large and real projects we have developed our own classification of ABC metric.

Clasification really makes sense when we talk about a class or method ABC metric.

According to ABC metric value obtained we can assert if it's necessary to refactor or in the other hand, the number of classes and methods are correct.

## Download

Version 1.9 (July, 15, 2014)

[Download](https://www.qalitax.com/descargas/product/sonar-abc-metric-plugin-1.9.jar?customerSurnames=customer&customerCompany=spanish&customerName=web_v2&customerEmail=customer@email.es)